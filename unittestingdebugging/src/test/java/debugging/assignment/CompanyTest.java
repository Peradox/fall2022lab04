package debugging.assignment;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit test Company class
 */
public class CompanyTest
{
    /**
     * Checks for different employees
     */
    @Test
    public void testDifferentEmployees()
    {
        Employee[] employees1 = new Employee[] { new Employee("Dan", 12345), new Employee("Gabriela", 123), new Employee("Andrew", 12)};

        Company c1 = new Company(employees1);

        // change it so that it's a different company

        Employee[] employees2 = new Employee[employees1.length];
            for(int i=0; i<employees1.length; i++){
                employees2[i]= new Employee(employees1[i].getName(),employees1[i].getEmployeeId());
            }

        employees2[0].setEmployeeId(12);

        Company c2 = new Company(employees2);
        assertNotEquals(c1, c2);

    }
}
